package pl.lpierog.util;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 * Created by lukasz.pierog on 18-May-18.
 */
public class TransferRequestFactory {

    public static Entity<Form> prepareRequest(String fromId, String toId, String amount) {
        MultivaluedMap<String, String> formData = new MultivaluedHashMap<>();
        formData.add("fromId", fromId);
        formData.add("toId", toId);
        formData.add("amount", amount);
        return Entity.form(formData);
    }
}
