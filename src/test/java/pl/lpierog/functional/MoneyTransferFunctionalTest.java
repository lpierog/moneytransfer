package pl.lpierog.functional;

import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pl.lpierog.exception.NonExistantAccountException;
import pl.lpierog.factory.AccountFactory;
import pl.lpierog.repository.AccountRepository;
import pl.lpierog.standalone.Application;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

/**
 * Created by lukasz.pierog on 30-Apr-18.
 */
public class MoneyTransferFunctionalTest {

    private UndertowJaxrsServer undertowJaxrsServer;

    @Before
    public void setUp()  {
        this.undertowJaxrsServer = Application.startServer();

        AccountFactory factory = new AccountFactory();
        factory.createAccount(BigDecimal.valueOf(1000), "1");
        factory.createAccount("2");
    }

    @After
    public void tearDown() {
        this.undertowJaxrsServer.stop();
        try {
            AccountRepository.removeAccount("1");
            AccountRepository.removeAccount("2");
        } catch (NonExistantAccountException e) {
            //expected if test failed
        }
    }

    @Test
    public void shouldFinishSuccessfullyWithCorrectParams() {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target("http://" + Application.HOST + ":" + Application.PORT + "/transfer");
        MultivaluedMap<String, String> formData = new MultivaluedHashMap<>();
        formData.add("fromId", "1");
        formData.add("toId", "2");
        formData.add("amount", "100");
        Response response = webTarget.request().post(Entity.form(formData));

        Assert.assertEquals(202, response.getStatus());
        Assert.assertEquals("Transfer finished successfully", response.readEntity(String.class));
    }

    @Test
    public void shouldFailWhenInsufficientBalance() {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target("http://" + Application.HOST + ":" + Application.PORT + "/transfer");
        MultivaluedMap<String, String> formData = new MultivaluedHashMap<>();
        formData.add("fromId", "1");
        formData.add("toId", "2");
        formData.add("amount", "10000");
        Response response = webTarget.request().post(Entity.form(formData));

        Assert.assertEquals(400, response.getStatus());
        Assert.assertEquals("Insufficient funds, request rejected", response.readEntity(String.class));
    }

    @Test
    public void shouldFailIfAccountDoesNotExist() {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target("http://" + Application.HOST + ":" + Application.PORT + "/transfer");
        MultivaluedMap<String, String> formData = new MultivaluedHashMap<>();
        formData.add("fromId", "1");
        formData.add("toId", "3");
        formData.add("amount", "100");
        Response response = webTarget.request().post(Entity.form(formData));

        Assert.assertEquals(400, response.getStatus());
        Assert.assertEquals("Cannot perform operation on non existent account, request rejected", response.readEntity(String.class));
    }

}
