package pl.lpierog.concurrent;

import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;
import org.junit.Assert;
import org.testng.annotations.*;
import pl.lpierog.exception.NonExistantAccountException;
import pl.lpierog.factory.AccountFactory;
import pl.lpierog.repository.AccountRepository;
import pl.lpierog.standalone.Application;
import pl.lpierog.util.TransferRequestFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

/**
 * Created by lukasz.pierog on 18-May-18.
 */
public class MoneyTransferBalancePreCheckFunctionalTest {

    private UndertowJaxrsServer undertowJaxrsServer;

    @BeforeClass
    public void startServer()  {
        this.undertowJaxrsServer = Application.startServer();
    }

    @AfterClass
    public void stopServer() {
        this.undertowJaxrsServer.stop();
    }

    @BeforeTest
    public void preTest()  {
        System.out.println("Initializing accounts...");
        AccountFactory factory = new AccountFactory();
        factory.createAccount(BigDecimal.valueOf(50), "1");
        factory.createAccount(BigDecimal.valueOf(100),"2");
        factory.createAccount(BigDecimal.valueOf(150),"3");
        factory.createAccount("4");
    }

    @AfterTest
    public void postTest() {
        System.out.println("Cleaning up...");
        try {
            AccountRepository.removeAccount("1");
            AccountRepository.removeAccount("2");
            AccountRepository.removeAccount("3");
            AccountRepository.removeAccount("4");
        } catch (NonExistantAccountException e) {
            //expected if test failed
        }
    }

    @Test(threadPoolSize = 10, invocationCount = 150)
    public void shouldEitherTransferOrReturn400() {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target("http://" + Application.HOST + ":" + Application.PORT + "/transfer");
        Response response = webTarget.request().post(TransferRequestFactory.prepareRequest("1", "4", "1"));
        response.close();
        assertCode200or400(response);

        Response response2 = webTarget.request().post(TransferRequestFactory.prepareRequest("2", "4", "1"));
        response2.close();
        assertCode200or400(response2);

        Response response3 = webTarget.request().post(TransferRequestFactory.prepareRequest("3", "4", "1"));
        response3.close();
        assertCode200or400(response3);
    }

    @Test(dependsOnMethods = "shouldEitherTransferOrReturn400")
    public void shouldEndUpWithCorrectBalanceOnEachAccount() throws Exception {
        Assert.assertEquals(0, AccountRepository.getAccount("1").getBalance().longValue());
        Assert.assertEquals(0, AccountRepository.getAccount("2").getBalance().longValue());
        Assert.assertEquals(0, AccountRepository.getAccount("3").getBalance().longValue());
        Assert.assertEquals(300, AccountRepository.getAccount("4").getBalance().intValue());
    }

    private void assertCode200or400(Response response) {
        Assert.assertTrue(response.getStatus() == 202 || response.getStatus() == 400);
    }
}
