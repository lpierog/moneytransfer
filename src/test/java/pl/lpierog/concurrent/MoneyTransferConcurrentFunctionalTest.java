package pl.lpierog.concurrent;

import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;
import org.junit.Assert;
import org.testng.annotations.*;
import pl.lpierog.exception.NonExistantAccountException;
import pl.lpierog.factory.AccountFactory;
import pl.lpierog.repository.AccountRepository;
import pl.lpierog.standalone.Application;
import pl.lpierog.util.TransferRequestFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

/**
 * Created by lukasz.pierog on 18-May-18.
 */
public class MoneyTransferConcurrentFunctionalTest {

    private UndertowJaxrsServer undertowJaxrsServer;
    private static final int invocationCount = 2000;

    @BeforeClass
    public void startServer()  {
        this.undertowJaxrsServer = Application.startServer();
    }

    @AfterClass
    public void stopServer() {
        this.undertowJaxrsServer.stop();
    }

    @BeforeTest
    public void preTest()  {
        System.out.println("Initializing accounts...");
        AccountFactory factory = new AccountFactory();
        factory.createAccount(BigDecimal.valueOf(100000), "5");
        factory.createAccount("6");
    }

    @AfterTest
    public void postTest() {
        System.out.println("Cleaning up...");
        try {
            AccountRepository.removeAccount("5");
            AccountRepository.removeAccount("6");
        } catch (NonExistantAccountException e) {
            //expected if test failed
        }
    }

    @Test(threadPoolSize = 10, invocationCount = invocationCount)
    public void shouldTransferSuccessfullyEachTime() {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target("http://" + Application.HOST + ":" + Application.PORT + "/transfer");
        Response response = webTarget.request().post(TransferRequestFactory.prepareRequest("5", "6", "10"));
        Assert.assertEquals(202, response.getStatus());
        Assert.assertEquals("Transfer finished successfully", response.readEntity(String.class));
        response.close();
    }

    @Test(dependsOnMethods = "shouldTransferSuccessfullyEachTime")
    public void shouldEndUpWithCorrectBalanceOnEachAccount() throws Exception {
        Assert.assertEquals(100000 - (10L * invocationCount), AccountRepository.getAccount("5").getBalance().longValue());
        Assert.assertEquals(10L * invocationCount, AccountRepository.getAccount("6").getBalance().longValue());
    }
}
