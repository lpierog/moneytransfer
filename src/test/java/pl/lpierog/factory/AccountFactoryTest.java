package pl.lpierog.factory;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pl.lpierog.exception.NonExistantAccountException;
import pl.lpierog.repository.AccountRepository;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Created by lukasz.pierog on 30-Apr-18.
 */
public class AccountFactoryTest {
    private static final String TEST_ACCOUNT_ID = "1";
    private AccountFactory sut;

    @Before
    public void setUp() {
        this.sut = new AccountFactory();
    }

    @After
    public void tearDown() {
        try {
            AccountRepository.removeAccount(TEST_ACCOUNT_ID);
        } catch (NonExistantAccountException e) {
            //expected if test failed
        }
    }

    @Test
    public void shouldAddAccountToRepository() throws Exception {
        this.sut.createAccount(BigDecimal.ZERO, TEST_ACCOUNT_ID);
        Assert.assertNotNull(AccountRepository.getAccount(TEST_ACCOUNT_ID));
    }

    @Test
    public void shouldSetInitialBalance() throws Exception {
        this.sut.createAccount(new BigDecimal(100), TEST_ACCOUNT_ID);
        Assert.assertEquals(0,
                AccountRepository.getAccount(TEST_ACCOUNT_ID).getBalance().compareTo(new BigDecimal(100)));
    }
}