package pl.lpierog.backend;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import pl.lpierog.exception.InsufficientFundsException;
import pl.lpierog.exception.NonExistantAccountException;
import pl.lpierog.model.Account;
import pl.lpierog.repository.AccountRepository;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Created by lukasz.pierog on 30-Apr-18.
 */
public class TransferServiceTest {

    private static final String FIRST_ACCOUNT_ID = "1";
    private static final String SECOND_ACCOUNT_ID = "2";

    private TransferService sut;
    private Account firstAccount;
    private Account secondAccount;

    @Before
    public void setUp() {
        this.firstAccount = new Account(new BigDecimal(1000), FIRST_ACCOUNT_ID);
        this.secondAccount = new Account(BigDecimal.ZERO, SECOND_ACCOUNT_ID);

        AccountRepository.addAccount(firstAccount);
        AccountRepository.addAccount(secondAccount);

        this.sut = new TransferService();
    }

    @After
    public void tearDown() throws Exception {
        AccountRepository.removeAccount(FIRST_ACCOUNT_ID);
        AccountRepository.removeAccount(SECOND_ACCOUNT_ID);
    }

    @Test
    public void shouldFinishSuccessfullyOnCorrectTransfer() throws Exception {
        sut.transfer(FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID, new BigDecimal(100));
    }

    @Test(expected = InsufficientFundsException.class)
    public void shouldThrowExceptionOnInsufficientFunds() throws Exception {
        sut.transfer(FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID, new BigDecimal(10000));
    }

    @Test
    public void shouldDecreaseBalanceFromSenderAccount() throws Exception {
        sut.transfer(FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID, new BigDecimal(100));
        Assert.assertTrue(areEqual(firstAccount.getBalance(), new BigDecimal(900)));
    }

    @Test
    public void shouldIncreaseBalanceOnReceiverAccount() throws Exception {
        sut.transfer(FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID, new BigDecimal(10.5));
        Assert.assertTrue(areEqual(secondAccount.getBalance(), new BigDecimal(10.5)));
    }

    @Test
    public void shouldNotDecreaseBalanceFromSenderAccountOnFailure() throws Exception {
        try {
            sut.transfer(FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID, new BigDecimal(10000));
        } catch (InsufficientFundsException e) {
            //expected
        }

        Assert.assertTrue(areEqual(firstAccount.getBalance(), new BigDecimal(1000)));
    }

    @Test
    public void shouldNotIncreaseBalanceOnReceiverAccountOnFailure() throws Exception {
        try {
            sut.transfer(FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID, new BigDecimal(10000));
        } catch (InsufficientFundsException e) {
            //expected
        }

        Assert.assertTrue(areEqual(secondAccount.getBalance(), BigDecimal.ZERO));
    }

    private boolean areEqual(BigDecimal firstValue, BigDecimal secondValue) {
        assert firstValue != null && secondValue != null;
        return firstValue.compareTo(secondValue) == 0;
    }

}