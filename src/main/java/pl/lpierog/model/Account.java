package pl.lpierog.model;

import java.math.BigDecimal;

/**
 * Created by lukasz.pierog on 30-Apr-18.
 */
public class Account {

    private final String id;
    private final Object lock;
    private BigDecimal balance;

    public Account(BigDecimal balance, String id) {
        this.balance = balance;
        this.id = id;
        this.lock = new Object();
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getId() {
        return id;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Object getLock() {
        return lock;
    }
}
