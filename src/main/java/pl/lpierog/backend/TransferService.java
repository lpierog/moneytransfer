package pl.lpierog.backend;

import org.apache.log4j.Logger;
import pl.lpierog.exception.InsufficientFundsException;
import pl.lpierog.exception.NonExistantAccountException;
import pl.lpierog.model.Account;
import pl.lpierog.repository.AccountRepository;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by lukasz.pierog on 30-Apr-18.
 */

public class TransferService {
    private static final Logger LOGGER = Logger.getLogger(TransferService.class);

    public void transfer(String fromId, String toId, BigDecimal amount)
            throws InsufficientFundsException, NonExistantAccountException {

        Account fromAccount = AccountRepository.getAccount(fromId);
        Account toAccount = AccountRepository.getAccount(toId);

        if (Objects.equals(fromAccount, toAccount)) {
            LOGGER.info("Attempt to transfer to the same account. No action will be taken.");
            return;
        }

        UUID transferId = UUID.randomUUID();

        LOGGER.info("Starting transfer operation " + transferId);
        Object lock1 = fromAccount.getId().compareTo(toAccount.getId()) < 0 ? fromAccount.getLock() : toAccount.getLock();
        Object lock2 = fromAccount.getId().compareTo(toAccount.getId()) < 0 ? toAccount.getLock() : fromAccount.getLock();
        synchronized (lock1) {
            synchronized (lock2) {
                if (ensureSufficientFunds(fromAccount, amount)) {
                    LOGGER.info("Transfer " + transferId + " has been accepted. From: "
                            + fromId + ", to: " + toId + ", amount: " + amount);
                    executeTransfer(fromAccount, toAccount, amount);
                    LOGGER.info("Finished successfully transfer operation " + transferId);
                } else {
                    LOGGER.info("Transfer " + transferId + " has been rejected - insufficient funds");
                    throw new InsufficientFundsException();
                }
            }
        }
    }

    private void executeTransfer(Account fromAccount, Account toAccount, BigDecimal amount) {
        BigDecimal senderBalanceAfterTransfer = fromAccount.getBalance().subtract(amount);
        fromAccount.setBalance(senderBalanceAfterTransfer);

        BigDecimal receiverBalanceAfterTransfer = toAccount.getBalance().add(amount);
        toAccount.setBalance(receiverBalanceAfterTransfer);
    }

    private boolean ensureSufficientFunds(Account account, BigDecimal amountToSubtract) {
        return account.getBalance().subtract(amountToSubtract).signum() != -1;
    }
}
