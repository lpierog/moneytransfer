package pl.lpierog.factory;

import org.apache.log4j.Logger;
import pl.lpierog.model.Account;
import pl.lpierog.repository.AccountRepository;

import java.math.BigDecimal;

/**
 * Created by lukasz.pierog on 30-Apr-18.
 */
public class AccountFactory {
    private static final Logger LOGGER = Logger.getLogger(AccountFactory.class);

    public Account createAccount(String id) {
        return createAccount(BigDecimal.ZERO, id);
    }

    public Account createAccount(BigDecimal initialBalance, String id) {
        LOGGER.info("Creating account. Id: " + id + ", initial balance: " + initialBalance);
        Account newAccount = new Account(initialBalance, id);
        AccountRepository.addAccount(newAccount);

        return newAccount;
    }
}
