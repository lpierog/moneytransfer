package pl.lpierog.standalone;

import io.undertow.Undertow;
import io.undertow.servlet.api.DeploymentInfo;
import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;
import org.jboss.resteasy.spi.ResteasyDeployment;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import pl.lpierog.api.TransferApi;
import pl.lpierog.factory.AccountFactory;

import java.math.BigDecimal;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by lukasz.pierog on 30-Apr-18.
 */
public class Application extends javax.ws.rs.core.Application {

    public static final String HOST = "0.0.0.0";
    public static final int PORT = 8080;

    public static void main(String... args) {
        startServer();

        //Prepare initial state for the example
        AccountFactory factory = new AccountFactory();
        factory.createAccount(BigDecimal.valueOf(1000), "1");
        factory.createAccount("2");
    }

    public static UndertowJaxrsServer startServer() {
        UndertowJaxrsServer server = new UndertowJaxrsServer();
        Undertow.Builder serverBuilder = Undertow.builder().addHttpListener(PORT, HOST);
        server.start(serverBuilder);

        ResteasyDeployment deployment = new ResteasyDeployment();
        deployment.setProviderFactory(new ResteasyProviderFactory());
        deployment.setApplicationClass(Application.class.getName());
        DeploymentInfo di = server.undertowDeployment(deployment)
                .setClassLoader(Application.class.getClassLoader())
                .setContextPath("/")
                .setDeploymentName("Money transfer application");
        server.deploy(di);

        return server;
    }

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new LinkedHashSet<>();
        resources.add(TransferApi.class);
        return resources;
    }
}
