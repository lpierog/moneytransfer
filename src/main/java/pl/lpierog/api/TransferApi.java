package pl.lpierog.api;

import org.apache.log4j.Logger;
import pl.lpierog.backend.TransferService;
import pl.lpierog.exception.InsufficientFundsException;
import pl.lpierog.exception.NonExistantAccountException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

/**
 * Created by lukasz.pierog on 30-Apr-18.
 */

@Path("/transfer")
public class TransferApi {
    private static final Logger LOGGER = Logger.getLogger(TransferApi.class);
    private final TransferService transferService;

    public TransferApi() {
        this.transferService = new TransferService();
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response transfer(@FormParam("fromId") String fromId, @FormParam("toId") String toId, @FormParam("amount") String amount) {
        LOGGER.info("Received REST command - transfer " + amount + " from " + fromId + " to " + toId);

        try {
            BigDecimal amountAsBigDecimal = new BigDecimal(amount);
            transferService.transfer(fromId, toId, amountAsBigDecimal);

            return Response.accepted("Transfer finished successfully").build();
        } catch (InsufficientFundsException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Insufficient funds, request rejected").build();
        } catch (NonExistantAccountException e) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("Cannot perform operation on non existent account, request rejected").build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Unhandled error occurred, please contact administrator").build();
        }
    }
}
