package pl.lpierog.repository;

import pl.lpierog.exception.DuplicateAccountIdentifierException;
import pl.lpierog.exception.NonExistantAccountException;
import pl.lpierog.model.Account;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Dummy class to hold all objects representing accounts, should be replaced with DB
 * Created by lukasz.pierog on 30-Apr-18.
 */
public class AccountRepository {
    private static final List<Account> accounts = new ArrayList<>();

    public static Account getAccount(String id) throws NonExistantAccountException {
        return accounts.stream()
                .filter(account -> account.getId().equals(id))
                .findFirst()
                .orElseThrow(NonExistantAccountException::new);
    }

    public static synchronized void addAccount(Account account) throws DuplicateAccountIdentifierException {
        if (verifyUniqueId(account)) {
            accounts.add(account);
        } else {
            throw new DuplicateAccountIdentifierException();
        }
    }

    private static boolean verifyUniqueId(Account newAccount) {
        return accounts.stream()
                .noneMatch(existingAccount -> Objects.equals(existingAccount.getId(), newAccount.getId()));
    }

    public static void removeAccount(String id) throws NonExistantAccountException {
        Account accountToRemove = getAccount(id);
        accounts.remove(accountToRemove);
    }

}
