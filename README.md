# MoneyTransfer

Simple implementation of REST API for money transfers between accounts.

Notes:

- Classes irrelevant for the case have been omitted, i.e. users, however typically every account would be connected to a person / enterprise entity
- Usually providing unique ids for the new account should be systems' responsibility, however here we provide them manually for easier testing
- this is actually not a RESTful API, because it does not meet the HATEOAS requirements, but implementing this would not be possible without largely extending the scope of this exercise

How to test

- default URL is http://localhost:8080/transfer
- API can be tested either by Postman/SoapUI or by starting pl.lpierog.functional.MoneyTransferFunctionalTest
- Instead of typical JSON input - http form is expected as this is easier to test
- main() method adds 2 pre-defined accounts, first with id "1" and 1000 credit, second with id "2" and 0 credit

![alt text](https://bitbucket.org/lpierog/moneytransfer/downloads/postman.JPG "Postman example")